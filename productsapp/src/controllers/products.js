const Product = require("../models/products");
/**
 * Products Controller
 */

const findAll = async () => {
  return Product.findAll();
};

/**
 * 
 * @param {string} product_id 
 * @returns {Promise<Product>}
 */
const getById = async (product_id) => {
  const prod = await Product.findByPk(product_id, {});
  if (!prod) {
    throw Error("Product does not exist");
  }
  return prod;
};

const createProduct = async (productDTO) => {
  return Product.create(productDTO, {});
};

const updateProduct = async (productDTO) => {
  const prod = await getById(productDTO.uuid);
  return prod.update(productDTO)
};

const deleteProduct = async (uuid) => {
  const prod = await getById(uuid);
  return prod.destroy({})
  // return Product.destroy({where:{uuid:uuid}})
};

module.exports = {
  findAll,
  getById,
  createProduct,
  updateProduct,
  deleteProduct,
};
