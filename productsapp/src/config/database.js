const { Sequelize } = require("sequelize");

// const sequelize = new Sequelize("sqlite::memory:");
const sequelize = new Sequelize(
  "products",
  process.env.MSQL_USER,
  process.env.MSQL_USER_PASSWORD,
  {
    host:'database',
    dialect: "mysql",
  }
);

const initDb = async () => {
  const Product = require("../models/products");

  await sequelize.sync({});

  Product.bulkCreate([
    { name: "Product A", price: 100, description: "Prod A" },
    { name: "Product B", price: 150, description: "Prod B" },
    { name: "Product C", price: 60, description: "Prod C" },
  ]);

  return connectDb();
};

const connectDb = () => {
  return sequelize.authenticate();
};

const dropDb = () => {};

module.exports = {
  sequelize,
  initDb,
  connectDb,
  dropDb,
};
