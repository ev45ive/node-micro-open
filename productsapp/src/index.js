const express = require("express");
const routes = require("./routes");
const { initDb } = require("./config/database.js");
const { broker } = require("./events");

const app = express();
// https://github.com/senchalabs/connect#middleware
app.use(express.json({}));
// app.use(express.static('/public'))

const host = process.env.APP_HOST || null,
  port = parseInt(process.env.APP_PORT) || 8080;

app.use("/api/", routes);

app.get("/", function (req, res) {
  res.send("<h1> Nothing to see here Products </h1>");
});

app.use(errorHandler);

initDb().then(() => {
  console.log("Database connected");
});


// Start the broker
broker.start();

app.listen(port, host, () => {
  console.log(`Listeninig on port ${port}`);
});

function errorHandler(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }
  console.error(err);
  // next(err)
  res.status(500);
  res.send(err);
  // res.render('error', { error: err })
}
