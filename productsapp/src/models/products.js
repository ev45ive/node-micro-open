const { Sequelize, Model, DataTypes, UUIDV4 } = require("sequelize");
const { sequelize } = require("../config/database");
const { broker } = require("../events");

class Product extends Model {}

Product.init(
  {
    uuid: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: UUIDV4,
      // https://www.npmjs.com/package/shortid
    },
    name: DataTypes.STRING(255),
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    price: DataTypes.INTEGER,
    photo: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    status: {
      type: DataTypes.ENUM,
      values: ["active", "pending", "deleted"],
    },
  },
  {
    sequelize,
    modelName: "products",
    hooks: {
      afterUpdate(product) {
        console.log("afterUpdate", product.toJSON());
        broker.emit("products.updated", product.toJSON());
      },
    },
  }
);

// Product.addHook("afterUpdate", "emitEvents", (product) => {
// });

module.exports = Product;
