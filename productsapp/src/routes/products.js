const { Router } = require("express");
const {
  findAll,
  getById,
  createProduct,
  updateProduct,
  deleteProduct,
} = require("../controllers/products");

const products = Router()
  // .param('product_id',function(req, res, next, id)=>{})
  // .use()
  /**
   * Get List of Products
   */
  .get("/", async (req, res, next) => {
    const filter = req.query["filter"];
    try {
      res.send(await findAll());
    } catch (err) {
      next(err);
    }
  })

  /**
   * Get Product By Id
   */
  .get("/:product_id", async (req, res, next) => {
    try {
      res.send(await getById(req.params["product_id"]));
    } catch (err) {
      next(err);
    }
  })

  /**
   * Create Product
   */
  .post("/", async (req, res) => {
    res.send(await createProduct(req.body));
  })

  /**
   * Update Product
   */
  .put("/:id", async (req, res) => {
    res.send(await updateProduct(req.body));
  })

  /**
   * Remove product
   */
  .delete("/:product_id", async (req, res) => {
    res.send(await deleteProduct(req.params["product_id"]));
  });

exports.products = products;
