var { Router } = require("express");
const { products } = require("./products");

const routes = (module.exports = Router());

routes.use('/products',products)
