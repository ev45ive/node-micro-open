npm init -y
npm i --save-dev nodemon 

# Typescript hints
npm i -g typescript
tsc --init

npm i --save-dev @types/node


# Node Event-Loop
https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/


- process.nextTick() fires immediately on the same phase
- setImmediate() fires on the following iteration or 'tick' of the event loop


# EventEmitter
https://nodejs.org/api/events.html#events_class_eventemitter
https://github.com/EventEmitter2/EventEmitter2#emitteremitasyncevent--eventns-arg1-arg2-

# Streams
https://nodejs.org/api/stream.html#stream_readable_streams

tail -f src/out.txt 

# Sequelize SQL ORM
https://sequelize.org/v5/

# UUID 
https://www.npmjs.com/package/shortid

# MIddleware
https://github.com/senchalabs/connect#middleware

# Docker registry
https://hub.docker.com/_/registry

# Windows 
winpty or powershell

# Images
https://hub.docker.com/_/busybox
https://hub.docker.com/_/alpine
https://hub.docker.com/_/ubuntu
https://hub.docker.com/_/redis
https://hub.docker.com/_/httpd
https://hub.docker.com/_/nginx
https://hub.docker.com/_/node
https://hub.docker.com/_/mariadb
https://hub.docker.com/_/postgres
https://hub.docker.com/_/memcached

# DockerFile
https://docs.docker.com/engine/reference/builder/#entrypoint
https://docs.docker.com/develop/develop-images/multistage-build/
https://codefresh.io/docker-tutorial/node_docker_multistage/

# Nodemon
https://medium.com/better-programming/docker-in-development-with-nodemon-d500366e74df
https://www.docker.com/blog/speed-up-your-development-flow-with-these-dockerfile-best-practices/
https://medium.com/create-a-server-with-nodemon-express-typescript/create-a-server-with-nodemon-express-typescript-f7c88fb5ee71

# Health 
https://docs.docker.com/engine/reference/builder/#healthcheck
https://expressjs.com/en/advanced/healthcheck-graceful-shutdown.html
https://github.com/gajus/lightship#lightship-usage-kubernetes-container-probe-configuration

# Volumes
https://stackoverflow.com/questions/41485217/mount-current-directory-as-a-volume-in-docker-on-windows-10
https://docs.docker.com/docker-for-windows/#file-sharing


# SQL
https://hub.docker.com/_/adminer/

# Best practices
https://expressjs.com/en/advanced/best-practice-performance.html

# Docker dev
docker build -t mynodedev -f Dockerfile.dev .

docker run -dit --rm -v ${PWD}:/home/node -p 8000:8080 mynodedev --name dev

docker logs dev -f


# API Gateway
https://www.express-gateway.io/

# 12 factor
https://12factor.net/pl/

# Libs
npm install body-parser multer @types/express @types/body-parser @types/multer class-transformer class-validator  

"emitDecoratorMetadata": true,
    "experimentalDecorators": true  

https://github.com/motdotla/dotenv
https://github.com/typeorm/typeorm

https://github.com/typestack/class-validator
https://github.com/typestack/class-transformer

https://github.com/inversify/InversifyJS

# DDD
https://martinfowler.com/bliki/DDD_Aggregate.html
https://stackoverflow.com/questions/1958621/whats-an-aggregate-root


# Microservices patterns
https://docs.microsoft.com/pl-pl/azure/architecture/patterns/
https://docs.microsoft.com/en-us/azure/architecture/patterns/

https://microservices.io/patterns/data/saga.html

https://microservices.io/patterns/data/transactional-outbox.html

# Events brokkers / Queues
https://www.rabbitmq.com/
https://kafka.apache.org/
https://redis.io/

# JS Micoservice brokers
Request response https://senecajs.org/
https://github.com/dashersw/cote
https://github.com/dashersw/cote#tracking-changes-in-the-system-with-a-publish-subscribe-mechanism

https://moleculer.services/
https://moleculer.services/docs/0.14/events.html

https://docs.nestjs.com/recipes/cqrs

https://www.npmjs.com/package/node-cqrs

# Logs
https://www.elastic.co/what-is/elk-stack

https://prometheus.io/
https://github.com/siimon/prom-client

https://grafana.com/
https://www.elastic.co/what-is/elk-stack

https://parseplatform.org/

