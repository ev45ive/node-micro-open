var fsp = require("fs").promises;
var fs = require("fs");
var path = require("path");
// var stream = require('stream');

var filePath = path.join(__dirname, "..", "config.txt");
var outFilePath = path.join(__dirname, "..", "out.txt");
var outFile2Path = path.join(__dirname, "..", "out2.txt");

(async () => {
  var rs = fs.createReadStream(filePath, {});
  var ws = fs.createWriteStream(outFilePath, {});

  rs.pipe(ws)
  rs.pipe(fs.createWriteStream(outFile2Path));

  // function pipe(ws){
  //   rs = this
  //   rs.on("data", (buf) => ws.write(buf.toString()));
  //   rs.on("end", () => ws.end());
  // }

  // Push (flowing)
  // rs.on("data", (buf) => ws.write(buf.toString()));
  // rs.on("end", () => ws.end());

  // // Pull (paused)
  // rs.on("readable", () => {
  //   // setTimeout(() => console.log(rs.read()), 1000);
  //   setTimeout(() => {
  //     var chunk = rs.read();
  //     if (chunk) {
  //       console.log(chunk);
  //       ws.write(chunk);
  //     } else {
  //       ws.end();
  //     }
  //   }, 1000);
  // });
})();

// (async () => {
//   var fd = await fs.open(filePath, "r+");
//   var buf = Buffer.from("Ala ma kota\n");
//   var pos = 0;

//   for (let i of [1, 2, 3, 4, 5]) {
//     await fd.write(buf, 0, buf.length, pos);
//     pos += buf.length;
//   }
// })();
