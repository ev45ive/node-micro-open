var fs = require("fs");
var path = require("path");

var filePath = path.join(__dirname, "buffers.js");

// var file = fs.readFileSync(filePath,'utf8');

var file = fs.readFile(filePath, { encoding: "utf8" }, (err, data) => {
  if (err) {
    console.error(err);
    return;
  }

  fs.readFile(
    path.join(__dirname, "config.txt"),
    { encoding: "utf8" },
    (err, data2) => {
      if (err) {
        console.error(err);
        return;
      }

      console.log(data, data2);
    }
  );
});
