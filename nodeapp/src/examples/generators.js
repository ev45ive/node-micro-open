var fs = require("fs").promises;
var path = require("path");
// var promisify = require('util').promisify

var dir = path.join(__dirname, ".");

async function* readDir(dir) {
  try {
    const files = await fs.readdir(dir);

    for (let file of files) {
      const filePath = path.join(dir, file);
      const stat = await fs.stat(filePath);

      if (stat.isDirectory()) {
        // allFiles.push(...(await readDir(filePath)));
        yield * await readDir(filePath)
      } else {
        const signal = yield file;
      }
    }
  } catch (err) {
    throw err;
  }
}
(async () => {
  const gen = readDir(dir);
  // console.log(await gen.next(1))
  // console.log(await gen.next(2))
  // console.log(await gen.next(3))

  for await (let file of gen) {
    console.log(file);
    // if (file.match(/.txt$/)) {
    //   break;
    // }
  }
})();

// readDir(dir)
//   .then((files) => files.filter((f) => f.match(/.js$/)))
//   .then((files) => {
//     console.log(files);
//   })
//   .catch(console.error);
