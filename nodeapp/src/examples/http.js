const http = require("http");

const server = http.createServer((req, res) => {
  
  var h = setInterval(() => {
    res.write("<div>Plackbr</div>");
  }, 100);

  setTimeout(() => {
    clearTimeout(h)
    // res.end();
  }, 5000);
});

server.on("clientError", (err, socket) => {
  socket.end("HTTP/1.1 400 Bad Request\r\n\r\n");
});

server.listen(8000, () => {
  console.log("Listening on port 8000");
});
