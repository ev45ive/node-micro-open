const { EventEmitter } = require("events");

const basicEmitter = new EventEmitter();

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

myEmitter.on("event", (...extras) => {
  console.log("an event occurred!",extras);
});

myEmitter.once('mam-placki', console.log)

myEmitter.emit('mam-placki','placki malinowe')
myEmitter.emit('mam-placki','placki babanowe')

setTimeout(() => {
  myEmitter.emit("event", "extra", "data");
}, 2000);


// {
//   "jesliSaPlacki":[
//     function(extra){},
//     function(extra){}
//   ]
// }