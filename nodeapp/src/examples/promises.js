
function echo(msg, cb){
  setTimeout(()=> cb(msg) ,1000)
}

echo('ala makota',console.log)


function echo(msg,fail){
  return new Promise((resolve,reject)=>{
    setTimeout(()=> fail? reject(fail) : resolve(msg) ,1000)
  })
}

echo('ala ma kota')
  .then(m => {
      return echo(m + ' i psa','upss..')  
  })
  .then(m => m + ' i rybki')
  .then(console.log)
  .catch(console.error)


async function testAsynvc(){    
    var m = await echo('Ala ma kota')
    
    console.log(m);

    return m + '!';
}
// undefined
testAsynvc()
// Promise {<pending>}
// VM2324:6 Ala ma kota


async function testAsynvc(){
  try{    
  var m = await echo('Ala ma kota','ups')
  
  console.log(m);

  return m + '!';

  }catch(err){
      console.error(err)
      return 'Fallback'
  }finally{
      console.log('finally')
  }
};

await testAsynvc();