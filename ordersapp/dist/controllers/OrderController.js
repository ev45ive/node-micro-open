"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderController = void 0;
const routing_controllers_1 = require("routing-controllers");
const events_1 = require("../events");
const random = ['Instance A', 'Instance B'][Math.floor(((Math.random() * 2) % 2))];
let lastUpdated = {};
// Define a service
events_1.broker.createService({
    name: "orders",
    events: {
        "products.updated": (ctx) => {
            console.log(ctx.id);
            console.log(ctx.nodeID);
            console.log(ctx.event);
            console.log(ctx.service);
            console.log(ctx.params);
            console.log(ctx.requestID);
            console.log(ctx.eventName);
            lastUpdated = ctx.params;
        },
    },
});
let OrderController = class OrderController {
    getAll() {
        return [
            "This action returns all orders ", random, lastUpdated
        ];
    }
    getOne(id) {
        return "This action returns user #" + id;
    }
    post(user) {
        return "Saving user...";
    }
    put(id, user) {
        return "Updating a user...";
    }
    remove(id) {
        return "Removing user...";
    }
};
__decorate([
    routing_controllers_1.Get("/orders"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "getAll", null);
__decorate([
    routing_controllers_1.Get("/orders/:id"),
    __param(0, routing_controllers_1.Param("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "getOne", null);
__decorate([
    routing_controllers_1.Post("/orders"),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "post", null);
__decorate([
    routing_controllers_1.Put("/orders/:id"),
    __param(0, routing_controllers_1.Param("id")), __param(1, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "put", null);
__decorate([
    routing_controllers_1.Delete("/orders/:id"),
    __param(0, routing_controllers_1.Param("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], OrderController.prototype, "remove", null);
OrderController = __decorate([
    routing_controllers_1.Controller('/api')
], OrderController);
exports.OrderController = OrderController;
