"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Application = void 0;
const express_1 = __importDefault(require("express"));
const routing_controllers_1 = require("routing-controllers");
const OrderController_1 = require("./controllers/OrderController");
class Application {
    constructor() {
        this.app = express_1.default();
        this.server = routing_controllers_1.createExpressServer({
            controllers: [
                OrderController_1.OrderController
            ]
        });
        // this.server.listen(8080)
    }
}
exports.Application = Application;
