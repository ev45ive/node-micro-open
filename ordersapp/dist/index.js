"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata"); // this shim is required
const dotenv_1 = __importDefault(require("dotenv"));
const app_1 = require("./app");
const events_1 = require("./events");
dotenv_1.default.config();
const app = new app_1.Application();
app.server.listen(8080);
// Start the broker
events_1.broker.start();
// catch ctrl+c event and exit normally
process.on('SIGINT', function () {
    console.log('Ctrl-C...');
    process.exit(2);
});
