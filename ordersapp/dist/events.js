"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.broker = void 0;
const moleculer_1 = require("moleculer");
// Create a ServiceBroker
exports.broker = new moleculer_1.ServiceBroker({
    transporter: "redis://redis:6379",
});
