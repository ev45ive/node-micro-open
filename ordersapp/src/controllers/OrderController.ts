import { Controller, Param, Body, Get, Post, Put, Delete } from "routing-controllers";
import { broker } from '../events'

const random = ['Instance A', 'Instance B'][Math.floor(((Math.random() * 2) % 2))]
let lastUpdated = {}

// Define a service
broker.createService({
   name: "orders",
   events: {
      "products.updated": (ctx) => {
         console.log(ctx.id)
         console.log(ctx.nodeID)
         console.log(ctx.event)
         console.log(ctx.service)
         console.log(ctx.params)
         console.log(ctx.requestID)
         console.log(ctx.eventName)
         lastUpdated = ctx.params
      },
   },
});

@Controller('/api')
export class OrderController {

   @Get("/orders")
   getAll() {
      return [
         "This action returns all orders ", random, lastUpdated
      ]
   }

   @Get("/orders/:id")
   getOne(@Param("id") id: number) {
      return "This action returns user #" + id;
   }

   @Post("/orders")
   post(@Body() user: any) {
      return "Saving user...";
   }

   @Put("/orders/:id")
   put(@Param("id") id: number, @Body() user: any) {
      return "Updating a user...";
   }

   @Delete("/orders/:id")
   remove(@Param("id") id: number) {
      return "Removing user...";
   }

}