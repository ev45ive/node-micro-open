import express, { Express } from 'express'
import { Server } from 'http'
import { createExpressServer } from "routing-controllers";
import { OrderController } from './controllers/OrderController';


export class Application {
  server: Server
  app: Express

  constructor() {
    this.app = express()

    this.server = createExpressServer({
      controllers: [
        OrderController
      ]
    })

    // this.server.listen(8080)
  }
}