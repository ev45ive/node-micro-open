import "reflect-metadata"; // this shim is required
import dotenv from 'dotenv'
import { Application } from './app'
import { broker } from "./events";


dotenv.config()

const app = new Application()


app.server.listen(8080)

// Start the broker
broker.start();

// catch ctrl+c event and exit normally
process.on('SIGINT', function () {
  console.log('Ctrl-C...');
  process.exit(2);
});