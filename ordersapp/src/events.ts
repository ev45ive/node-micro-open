import { ServiceBroker } from 'moleculer'

// Create a ServiceBroker
export const broker = new ServiceBroker({
  transporter: "redis://redis:6379",
});


